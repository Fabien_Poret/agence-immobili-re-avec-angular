import { Component, OnInit, OnDestroy } from '@angular/core';
import { PropertiesService } from '../services/properties.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  properties = [];

  // Permet de nous abonner au subject de properties dans le service
  propertiesSubscription : Subscription;
  
  getSoldValue(sold) {
    if(sold){
      return 'red';

    } else{
      return 'green';
    }
  }

  //Je récupère ma classe propertiesService depuis le constructor
  constructor(
    private propertiesService: PropertiesService,
    ) { 
  }

  // ngOnInit c'est la fonction qui est excécuté au chargement du module
  ngOnInit(): void {
    //Permet de récuperer la méthode propertiesSubject depuis la classe propertiesService
    // je dois m'abonner à l'observable
    // Il ne faut pas oublier de se désabonner
    this.propertiesSubscription = this.propertiesService.propertiesSubject.subscribe(
      // Je type ma data
      (data: any) => {
        this.properties = data;
      }
    );
    // Je récupère les properties dans ma bdd 
    this.propertiesService.getProperties();
    
    // J'indique à l'émetteur de transmettre les données
    this.propertiesService.emitProperties();
  }

  ngOnDestroy(){
    // Je me désabonne pour éviter les bugs
    console.log('destroy');
    this.propertiesSubscription.unsubscribe();
  }

}
