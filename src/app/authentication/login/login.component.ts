import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Je type mon loginForm 
  loginForm : FormGroup;

  errorLogin : '';

  // Je récupère mon formBuilder et mon service auth 
  constructor(
    private formBuilder: FormBuilder,
    private authentication: AuthenticationService,
    private router: Router
    ) {
  }

  ngOnInit(): void {
    this.initLoginForm();
  }

  initLoginForm(){
    // Permet de créer un JSON à partir de mon form
  
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmitLoginForm(){
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this.authentication.login(email, password).then(
      (data) => {
        // Permet de faire une redirection vers /admin/dashboard 
        // Le deuxième paramètre de navigate ne prend pas de / 
        this.router.navigate(['/admin', 'dashboard']) // /admin/dashboard
      }
    ).catch(
      (error) => {
        console.log(error);
        this.errorLogin = error;
      }
    );
  }

}
