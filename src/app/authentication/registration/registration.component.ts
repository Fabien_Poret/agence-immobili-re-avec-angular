import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

 // Je type mon loginForm 
 registrationForm : FormGroup;

 // Je récupère mon formBuilder et mon service auth 
 constructor(
   private formBuilder: FormBuilder,
   private authentication: AuthenticationService,
   private router: Router
   ) {
 }

 ngOnInit(): void {
   this.initRegistrationForm();
 }

 initRegistrationForm(){
   // Permet de créer un JSON à partir de mon form
 
   this.registrationForm = this.formBuilder.group({
     email: ['', [Validators.required, Validators.email]],
     password: ['', [Validators.required, Validators.minLength(6)]]
   });
 }

 onSubmitRegistrationForm(){
   const email = this.registrationForm.get('email').value;
   const password = this.registrationForm.get('password').value;
   this.authentication.registration(email, password).then(
     (data) => {
       // Permet de faire une redirection vers /admin/dashboard 
       // Le deuxième paramètre de navigate ne prend pas de / 
       this.router.navigate(['/admin', 'dashboard']) // /admin/dashboard
     }
   ).catch(
     (error) => {
       console.log(error);
     }
   );
 }
}
