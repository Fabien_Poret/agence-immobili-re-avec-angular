import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'monAgenceAngular';
  
  constructor(){
    // Je configure firebase dans mon composant racine pour que ça communique avec l'ensemble des mes autres composants (via héritage)
    const firebaseConfig = {
      apiKey: "AIzaSyAwjYdqXvrKjod8CUEoPQjqGukAbzqZ5g4",
      authDomain: "monagenceangular-416f6.firebaseapp.com",
      databaseURL: "https://monagenceangular-416f6.firebaseio.com",
      projectId: "monagenceangular-416f6",
      storageBucket: "monagenceangular-416f6.appspot.com",
      messagingSenderId: "101424210630",
      appId: "1:101424210630:web:d6a4273321e9d6b289adc7"
    };
    firebase.initializeApp(firebaseConfig);
  }
}
