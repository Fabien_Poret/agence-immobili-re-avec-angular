import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegistrationComponent } from './authentication/registration/registration.component';
import { HistoryComponent } from './history/history.component';
import { SinglePropertyComponent } from './single-property/single-property.component';
import { AuthGuardService } from './services/auth-guard.service';

// Permet de lier nos composants à des routes
const routes: Routes = [
  {
    path : 'home',
    component :  HomeComponent
  },
  {
    path : 'admin/dashboard', // Localhost:4200/admin/dashboard
    // Je regarde si mon utlisateur est connecté 
    canActivate: [AuthGuardService],
    component :  AdminDashboardComponent
  },
  {
    path : 'login', 
    component :  LoginComponent
  },
  {
    path : 'registration', 
    component :  RegistrationComponent
  },
  {
    path : 'history', 
    component :  HistoryComponent
  },
  {
    // permet d'avoir une URL dynamique 
    path : 'property/:id',
    component : SinglePropertyComponent
  },
  // Si le chemin est vide alors rediriger vers home 
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  // si le chemin n'est pas connu alors rediriger vers home 
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
