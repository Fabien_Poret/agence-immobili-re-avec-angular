import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PropertiesService } from 'src/app/services/properties.service';
import { Subscription } from 'rxjs';
import * as $ from 'jquery';
import { Property } from 'src/app/interface/property';

@Component({
  selector: 'app-admin-properties',
  templateUrl: './admin-properties.component.html',
  styleUrls: ['./admin-properties.component.css']
})
export class AdminPropertiesComponent implements OnInit {

  // Je dis que propertiesForm est de type FormGroup à importer depuis angular form
  propertiesForm: FormGroup;

  // Permet de nous abonner au subject de properties dans le service
  propertiesSubscription: Subscription;
  properties: any[] = [];

  indexToRemove;
  indexToUpdate;
  editMode = false;

  photoUploading = false; 
  photoUploaded = false;
  photosAdded: any[] = [];

  // Nécessaire pour construire mon formulaire need service 
  constructor(private formBuilder: FormBuilder, private propertiesService: PropertiesService) { }

  ngOnInit(): void {
    this.initPropertiesForm();
     //Permet de récuperer la méthode propertiesSubject depuis la classe propertiesService
    // je dois m'abonner à l'observable
    // Il ne faut pas oublier de se désabonner
    this.propertiesSubscription = this.propertiesService.propertiesSubject.subscribe(
      // Je type ma data
      (data: Property[]) => {
        this.properties = data;
      }
    );
    // Je récupère les properties dans ma bdd 
    this.propertiesService.getProperties();

    // J'indique à l'émetteur de transmettre les données
    this.propertiesService.emitProperties();
    console.log(this.properties);

  }

  initPropertiesForm(){
    // Permet de créer un JSON à partir de mon form
    this.propertiesForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(6)]],
      category: ['', Validators.required ],
      surface: '',
      rooms: '',
      description:'',
      price: ['', Validators.required ],
      sold: ''
    })
  }

  // -----------------Méthode reactive----------------------
  // La méthode s'active à la soumission de mon formulaire
  onSubmitPropertiesForm(){
    // Mon form est de type Property Interface 
    // ça me permet de controller les champs de mon form 
    const newProperty: Property = this.propertiesForm.value;
    
    // Je dis que si sold est undefined alors indiquer false 
    // C'est pour éviter les erreurs avec database car il n'ajoute pas les undefined 
    newProperty.sold = this.propertiesForm.get('sold').value ? this.propertiesForm.get('sold').value : false;

    // C'est égal à this.photourl uniquement si elle existe, sinon elle est vide 
    newProperty.photos = this.photosAdded ? this.photosAdded : [];

    if (this.editMode) {
      // J'envoie ma nouvelle property à la méthode createProperty de mon service propertiesService
      this.propertiesService.updateProperty(newProperty, this.indexToUpdate);
    } else {
      // J'envoie ma nouvelle property à la méthode createProperty de mon service propertiesService
      this.propertiesService.createProperty(newProperty);
    }
    // Je ferme la fenêtre modal
    $('#propertiesFormModal').modal('hide');
  }

  onDeleteProperty(index){
    // Je récupère mon index depuis ma boucle dans admin properties et je la passe à ma variable this.indexToRemove
    this.indexToRemove = index;
  }

  // Supprimer un élément de properties
  onConfirmDeleteProperty(){
  
    // Suppression de la photo 
    // if (this.properties[this.indexToRemove].photo && this.properties[this.indexToRemove].photo !=='') {
    //   this.propertiesService.removeFile(this.properties[this.indexToRemove].photo);
    // }

    this.properties[this.indexToRemove].photos.forEach(
      (photo) => {
        this.propertiesService.removeFile(photo);
      }
    )

    // J'appel la fonction deleteproperty de mon service
    this.propertiesService.deleteProperty(this.indexToRemove); 
     // Je ferme la fenêtre modal
     $('#propertiesDeleteModal').modal('hide');
  }

  // Je modifie mon property 
  // Je type mon property pour éviter les erreurs 
  onEditProperty(property : Property){
    // On passe en mode édition
    this.editMode = true;
    // J'ouvre la modal propertiesFormModal
    $('#propertiesFormModal').modal('show');
    // Je récupère mes valeurs et je les passes à mon form
    this.propertiesForm.get('title').setValue(property.title);
    this.propertiesForm.get('category').setValue(property.category);
    this.propertiesForm.get('surface').setValue(property.surface);
    this.propertiesForm.get('rooms').setValue(property.rooms);
    this.propertiesForm.get('description').setValue(property.description ? property.description : '');
    this.propertiesForm.get('price').setValue(property.price);
    this.propertiesForm.get('sold').setValue(property.sold);

    this.photosAdded = property.photos ? property.photos : [];

    // Je récupère mon index à partir de ma property 
    const index = this.properties.findIndex(
      (propertyEl) => {
        if (propertyEl === property) {
          return true;
        }
      }
    )
    // Je passe mon index à mon index à update 
    this.indexToUpdate = index;

  }


  // La méthode resetForm est appelé quand on click sur Ajouter un bien depuis le composant admin-properties 
  // Cela sert à éviter que le form garde les anciennes données
  resetForm(){
    // On passe en mode par défaut création 
    this.editMode = false;

    // Je reset mon form avec la méthode reset
    this.propertiesForm.reset();

    // Je reset la photo url 
    this.photosAdded = [];
  }

  // Gestion de l'upload de mon file 
  onUploadFile(event){
    this.photoUploading = true;
    this.propertiesService.uploadFile(event.target.files[0]).then(
      (url: string) => {
        this.photosAdded.push(url);
        this.photoUploading = false;
        this.photoUploaded = true;
        setTimeout(() => {
          this.photoUploaded = false;
        }, 5000)
      }
    );
  }
  
  onRemoveAddedPhoto(index){
    this.propertiesService.removeFile(this.photosAdded[index]);
    this.photosAdded.splice(index, 1);
  }
}
