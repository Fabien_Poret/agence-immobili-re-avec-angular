// Model de donnée de Property qui permet de définir des paramètres précis d'un ensemble de donnée 
export interface Property {
    // Le ? permet de ne pas rendre le champ obligatoire 
    title: string;
    category: string;
    surface?: string;
    rooms?: string;
    description?: string;
    price: string;
    sold?: boolean;
    photos?: any[];
}
