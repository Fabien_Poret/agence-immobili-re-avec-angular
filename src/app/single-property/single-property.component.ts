import { Component, OnInit } from '@angular/core';
import { PropertiesService } from '../services/properties.service';
import { ActivatedRoute } from '@angular/router';
import { Property } from '../interface/property';

@Component({
  selector: 'app-single-property',
  templateUrl: './single-property.component.html',
  styleUrls: ['./single-property.component.css']
})
export class SinglePropertyComponent implements OnInit {

  property: Property;

  constructor(
    private propertiesService: PropertiesService,
    // Permet de récupérer l'id de la route 
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    // Récupère l'id dans l'url 
    const id = this.route.snapshot.paramMap.get('id');
    this.propertiesService.getSingleProperties(id).then(
      (property: Property) => {
        this.property = property;
      }
    ).catch(
      (error) => {
        console.error(error);
      }
    );
  }

}
