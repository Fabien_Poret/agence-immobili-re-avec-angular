import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private toastr: ToastrService) { }

  registration(email: string, password: string){
    return new Promise(
      (resolve, reject) => {
        // le .then est quand ça c'est bien passé 
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => {
            console.log('Inscrit');
            this.toastr.success('Bienvenu parmis nous', email);
            resolve();
          }
        ).catch(
          (error) => {
            this.toastr.error('Vous n\'êtes pas encore le bienvenu', email);
            reject(error);
          }
        )
      }
    )
  }

  login(email: string, password: string){
    return new Promise(
      (resolve, reject) => {
        // le .then est quand ça c'est bien passé 
        firebase.auth().signInWithEmailAndPassword(email, password).then(
          (data) => {
            this.toastr.success('Vous êtes bien connecté sur le site de l\'agence', email);
            resolve(data);
          }
        ).catch(
          (error) => {
            this.toastr.error('Vous n\'êtes pas connecté sur le site de l\'agence', email);
            reject(error);
          }
        )
      }
    )
  }

 logout(){
    this.toastr.warning('À très bientôt !');
    return firebase.auth().signOut();
 }

}
