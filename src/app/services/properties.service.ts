import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Property } from '../interface/property';
import * as firebase from 'firebase';
import { ToastrService } from 'ngx-toastr';

// Le service permet de récuperer les données (properties) de la BDD

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {
 
  constructor(private toastr: ToastrService) { }

  // Je dis que ma donnée fait référence à l'interface Property et j'ajoute également que c'est un tableau 
  // Permet d'éviter l'erreur 'le tableau est undefined' 
  properties: Property[] = [];

  // ------------------Observable ---------------
  // Je créer un émetteur qui observera les données de properties
  propertiesSubject = new Subject<Property[]>();

  //Permet d'envoyer les données (remplace getProperties)
  emitProperties(){
    // J'envoie les données à mon subject
    this.propertiesSubject.next(this.properties);
  }

  // Je sauvegarde mes éléments dans ma BDD 
  saveProperties(){
    // J'accède à ma database de firebase 
    // Le ref va prendre le chemin, la référence 
    firebase.database().ref('/properties').set(this.properties);
    console.log('ok');
    
  }

  getProperties(){
    // Le on permet d'avoir une récupération des données en temps réel  
    // Il écoute la BDD pour récuperer les modifications 
    firebase.database().ref('/properties').on('value', 
    (data) => {
      // Je récupère les données de data.val si elle existe sinon je lui renvoi un tableau vide 
      this.properties = data.val() ? data.val() : []; 
      this.emitProperties();
    }
    );
  }

  // Je récupère un seul bien 
  getSingleProperties(id){
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/properties/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }
        ).catch(
          (error) => {
            reject(error);
          }
        );
      }
    )
  }

  createProperty(property : Property){
    this.properties.push(property);
    // Je sauvegarde les données de mon property dans la base de donnée firebase 
    this.saveProperties();
    // Je n'oublie pas de faire un emit pour éviter les bugs 
    this.emitProperties();
    this.toastr.success(property.title, 'Le bien est ajouté');
  }

  deleteProperty(index){
    // J'enleve la property de index X 
    this.properties.splice(index, 1);
    // Je sauvegarde les données de mon property dans la base de donnée firebase 
    this.saveProperties();
    // Je met à jour mon écouteur
    this.emitProperties();
    this.toastr.warning('Le bien est supprimé');
  }
  
  updateProperty(property : Property, index){
    // La property ayant l'index X est = à la nouvelle property
    this.properties[index] = property;
    // Je sauvegarde les données de mon property dans la base de donnée firebase 
    this.saveProperties();
    // Je met à jour mon écouteur
    this.emitProperties();
    this.toastr.info('Le bien est modifié', property.title);
  }

  uploadFile(file: File){
    // Promise = traitement de façon asynchrone 
    return new Promise(
      (resolve, reject) => {
        // Je génère un nom de fichier unique 
        const uniqueId = Date.now().toString();
        const fileName = uniqueId + file.name;
        const upload = firebase.storage().ref().child('images/properties/' + fileName).put(file);

        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          // Pending 
          () => {
            console.log('Chargement ...');
          },
          // Error
          (error) => {
            console.log(error);
            reject(error);
          },
          // Success
          () => {
            upload.snapshot.ref.getDownloadURL().then(
              (downLoadUrl) => {
                resolve(downLoadUrl);
              }
            )
          }
        );
      }
    );
  }

  removeFile(fileLink: string){
    if(fileLink){
      return new Promise(
        (resolve, reject) => {
          const storageRef = firebase.storage().refFromURL(fileLink);
          storageRef.delete().then(
            () => {
              console.log('File deleted');
              resolve();
            }
          ).catch(
            (error) => {
              console.error(error);
            }
          );
        }
      )
    }
    
  }
}
