import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userState = false;

  constructor(
    private authentification: AuthenticationService,
    private router: Router
    ) { }

  // PropertyBinding : Modification de l'affichage en fonction de paramètre
  title = 'Mon agence immobilière';
  // isDisabled = true;
  
  ngOnInit(): void {
    // On écoute le statut d'authentification de l'utilisateur, dès que celui ci va changer, cette fonction est appelé 
    firebase.auth().onAuthStateChanged(
      // nous retourne une session user 
      (userSession) => {
        if(userSession) {
          // Si l'utilisateur possède une session alors l'état est true
          this.userState = true;
          console.log('Connecté !');
        } else {
          this.userState = false;
          console.log('Déconnecté');
        }
      }
    )
  }

  onSignOut(){
    this.authentification.logout().then(
      (data) => {
        console.log('logout');
        this.router.navigate(['/']);
      }
    ).catch(
      (error) => {
        console.log(error);
      }
    )
  }
  // EventBinding : Gestion des événements depuis l'affichage
  // onClick(){
  //   return this.isDisabled = false;
  // }
}
